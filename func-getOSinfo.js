/**
* Created by rainer on 2016-08-26.
*
* file: func-getOSinfo.js
*
* creates an object of local machine information
*
*/


var os = require('os');
module.exports = getOSinfo;

function getOSinfo(x){
    var ldAvg = os.loadavg();

    var osData = {
        type:       os.type(),
        release:    os.release(),
        arch:       os.arch(),
        hostname:   os.hostname(),
        homedir:    os.homedir(),
        uptime:     parseInt( os.uptime() / 3600 ).toString() + " hours",
        freemem:    parseInt( os.freemem() / 1024 / 1024 ).toString() + "MB",
        totalmem:   parseInt( os.totalmem() / 1024 / 1024 ).toString() + "MB",
        load:       ldAvg,
        loadtrunc:  [ ldAvg[0].toFixed(2),ldAvg[1].toFixed(2), ldAvg[2].toFixed(2) ],
        numCPUs:    os.cpus().length,

        cpus:       os.cpus(),
        network:    os.networkInterfaces()

    };

/*
    console.log("arch", osData.type + ": " +  osData.release  + " - " + osData.arch );
    console.log("CPUs", osData.numCPUs + " type: " + osData.cpus[0]. model + " speed (MHz): " + osData.cpus[0].speed);
    console.log("host", osData.hostname + " " +  osData.homedir );
    console.log("uptime", osData.uptime);
    console.log("operating", "memory: " + osData.freemem + " / " + osData.totalmem + "; load: " + osData.load );
*/
    return osData;
}
