#!/usr/bin/env node

/**
 * Created by rainer on 2016-08-25.
 */

//"use strict";

//* set up libraries and get local machine information
var os = require('os');
var http = require('http');
var eddystoneBeacon = require('eddystone-beacon');
var bleno = require('bleno');
//var osData = require("./func-getOSinfo");

//* set up eddystone libraries
var options = {
    name: 'Aditude-Media',  // set device name when advertising (Linux only)
    txPowerLevel: -18,      // override TX Power Level, default value is -21,
};

console.log("http waiting");


//* set up an http server to respond to only two types of request from the localhost
http.createServer(function(request, response) {
    var headers  = request.headers;
    var method   = request.method;
    var url      = request.url;
    var resp       = null;

    //* build response message
    response.setHeader("Access-Control-Allow-Origin", headers.origin );
    response.setHeader('Content-Type', 'application/json');
    response.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST');
    response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");


    //* check for error condition and report
    request.on('error', function(err) {
        console.error(err);
        response.statusCode = 500;
    });

    //* get os information and send back to requester
    if( url === "/environment" && method === "GET" ){
        resp = getOSinfo();
        response.statusCode = 200;
    }

    //* set BLE eddystone address
    if( method === "POST"){


        var frag = url.split("?q=");
        if( frag[0] === "/set-eddystone" ){
            var eddy = "https://" + frag[1];

            var test = eddystoneBeacon.advertiseUrl( eddy, options );
             if( bleno.state === "poweredOn"){
             response.statusCode = 202;
             }else{
             response.statusCode = 503;
             }
        }

    }

    response.end(JSON.stringify(resp))

}).listen(3020);





function getOSinfo(x){
    var ldAvg = os.loadavg();

    return {
        type:       os.type(),
        release:    os.release(),
        arch:       os.arch(),
        hostname:   os.hostname(),
        homedir:    os.homedir(),
        uptime:     parseInt( os.uptime() / 3600 ).toString() + " hours",
        freemem:    parseInt( os.freemem() / 1024 / 1024 ).toString() + "MB",
        totalmem:   parseInt( os.totalmem() / 1024 / 1024 ).toString() + "MB",
        load:       ldAvg,
        loadtrunc:  [ ldAvg[0].toFixed(2),ldAvg[1].toFixed(2), ldAvg[2].toFixed(2) ],
        numCPUs:    os.cpus().length,

        cpus:       os.cpus(),
        network:    os.networkInterfaces()

    };
}