# README #

Aditude Media's implementation of an Eddystone Beacon
Version 1


## What is this repository for? ##

This repository holds the node.js code that runs as an Eddystone Beacon.  In order to use this code, three things must be set up first:


Step 1: For Ubuntu running on an Intel Compute Stick or equivalent, the Bluetooth libraries need to be added:

    sudo apt-get install bluetooth bluez libbluetooth-dev libudev-dev


Step 2: Install node.js:

    curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
    sudo apt-get install -y nodejs

Make sure node is on your path, if it's not, one option is to symlink nodejs to node:

    sudo ln -s /usr/bin/nodejs /usr/bin/node


Step 3: Navigate to the directory that you want to run the beacon from and then install the third party app [ Bleno ](https://github.com/sandeepmistry/bleno):

    npm install bleno


Step 4: Load the [ Eddystone ] (https://github.com/don/node-eddystone-beacon) library.  This library uses [ Bleno ](https://github.com/sandeepmistry/bleno) to access the Bluetooth hardware.

    npm install eddystone-beacon


Step 5: Finally install our beacon code into the same directory as Eddystone and Bleno.

## Who do I talk to? ##

* Created by Rainer Paduch
* Other contact is Maher Awad

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)